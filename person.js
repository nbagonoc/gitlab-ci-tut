module.exports = class Person {
    who() {
        let persons = ['Father', 'Mother', 'Brother', 'Sister'];
        let res = `
                    <html>
                        <head>
                            <meta charset="UTF-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <title>Gitlab CI Sandbox</title>
                        </head>
                            <body>
                                <h1>I am your ${persons[Math.floor(Math.random()*persons.length)]}</h1>
                                <iframe src="https://giphy.com/embed/ASvQ3A2Q7blzq" width="480" height="392" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
                            </body>
                    </html>
                `;
        
        // console.log(res)
        return res;
    }
}