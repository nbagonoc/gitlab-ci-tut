const fs = require('fs');
const Person = require('./person');

const func = new Person();
let who = func.who();
let path = process.env.EXPORT_PATH ? process.env.EXPORT_PATH : `./public/export.html`;

fs.writeFile(
    path,
    who,
    err => {
        if (err) throw new Error(`Can't create an export file: (${path})`);
    }
);

console.log(path)
